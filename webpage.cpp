#include <iostream>
#include <cstring>
#include <cstdlib>
#include <fstream>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <vector>

int main() 
{
	std::string url = "naurunappula.com";
	std::string command = "wget --quiet -O - " + url;
	//command.append(url);
    FILE *fp=popen(command.c_str(),"r");
	const int size = 10000;
	char buffer[size] = {'\0'};

	// Putting lines to vector
	std::string temp = "";
	std::vector<std::string> html;
	while (!feof(fp))
	{
		if (fgets (buffer, size, fp) != NULL)
		{
			//fputs(buffer, stdout);
			for (int i = 0; i < size; ++i)
			{
				// End of buffer
				if (buffer[i] == '\0')
				{
					if (temp != "")
						html.push_back(temp);
					break;
				}
			
				if ((buffer[i] == '\r' || buffer[i] == '\n') && temp != "")
				{
					// End of line. Putting the string that we got so far to the vector
					html.push_back(temp);
					temp = "";
				}
				else if (buffer[i] != '\r' && buffer[i] != '\n')
				{
					// Putting char to string
					temp += buffer[i];
				}
			}
		}
	}
	
	// Now that we have the html source in the vector, we will search for the title from it
	for (int i = 0; i < html.size(); ++i)
	{
		int index = html.at(i).find("<title>");
		if (index != std::string::npos)
		{
			// Looking for the end of the title "</title>"
			for (int j = i; j < html.size(); ++j)
			{
				int index2 = html.at(j).find("</title>");
				if (index2 != std::string::npos)
				{
					// We have the start and end line of the title
					// Putting lines to one
					std::string titleLine = "";
					for (int k = i; k <= j; ++k)
					{
						std::string temp = html.at(k);
						// Removing \n and \r
						while (temp.at(temp.size() - 1) == '\n'
								|| temp.at(temp.size() - 1) == '\r')
						{
							temp.erase(temp.size() - 1);
						}
						titleLine += temp + " ";
					}
					// Removing <title>
					index = titleLine.find("<title>");
					//titleLine = trim(titleLine);
					if ((index + 7) > titleLine.size()) // If there is nothing after <title>
						return 0;
					titleLine = titleLine.substr(index + 7);
					
					// Removing </title>
					index2 = titleLine.find("</title>");
					//titleLine = trim(titleLine);
					if (index2 == std::string::npos) // No title found, atleast nothing good
						return 0;
					titleLine = titleLine.substr(0, index2);
					
					
					std::cout << titleLine << std::endl;
				}
			}
			// no </title> was found, returning nothing
		}
	}
	
	/*
	// Printing vector
	for (int i = 0; i < html.size(); ++i)
	{
		cout << html.at(i) << endl;
	}
	
	cout << "Size of vector: " << html.size() << endl;
	*/
	pclose(fp); //close pipe
    return 0;
}

