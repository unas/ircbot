#include <algorithm>
#include <arpa/inet.h>
#include <ctime>
#include <cstdlib>
#include <cstring>
#include <errno.h>
#include <fcntl.h>
#include <fstream>
#include <iostream>
#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>
#include <vector>
#include "ircbot.h"

namespace IB
{
	#include "functions.h"

	void IrcBot::parseLine()
	{
		// put line in vector, give to different methods to check it out
		std::vector<std::string>lines = str2vec(ircText_, '\n');
		// Goes through every line that the server sent to us
		for (int i = 0; i < lines.size(); ++i)
		{
			shouldWeKickSomeone();
			if (otherBotTalking(lines.at(i))) continue;
			if (wasIKicked(lines.at(i))) continue; // Did someone kick the bot?
			//if (checkForPing()) continue; // Was it another ping we missed earlier?
			if (userLeft(lines.at(i))) continue; // Did someone leave?
			if (userJoined(lines.at(i))) continue; // Did someone join the channel?
			if (userChangedNick(lines.at(i))) continue; // Did someone change their nick?
			// If someone pasted a link, then we will say Wanha if it is old
			// If the link is not old, we will tell the channel what the topic is of that page
			linkPasted(lines.at(i)); // Did someone paste a link?
			if (weGotOperatorStatus(lines.at(i))) continue; // Did we get operator status?
			if (weLostOperatorStatus(lines.at(i))) continue; // Did we lose our operator status?
			if (l4d(lines.at(i))) continue; // Is someone asking for a game of l4d
			if (cs(lines.at(i))) continue; // Is someone asking for a game of cs:go
			if (karma(lines.at(i))) continue;
			if (wanha(lines.at(i))) continue; // Is someone asking for their wanhapoints
			if (userTalkingToBot(lines.at(i))) continue; // If someone is talking directly to our bot
		}
	}
	
	std::string IrcBot::getWhatUserSaidToChannel(std::string line)
	{
		int index = line.find("PRIVMSG " + channel_ + " :");
		if (index == std::string::npos) // If no one said anything to the channel. Line was something else
			return "";
		
		// Parsing the text that someone said
		index += 8 + channel_.size() + 2;
		
		if (line.size() <= index) // Making sure that there is some text after the " :"
				return "";
		std::string userSaidToUs = line.substr(index);
			
		// Cleaning the line, trim, lower
		userSaidToUs = cleanLine(userSaidToUs);
		
		return userSaidToUs;
	}
	
	bool IrcBot::userTalkingToBot(std::string line)
	{
		// Checking if someone is talking to the bot on the channel
		int index = line.find("PRIVMSG " + channel_ + " :" + user_ + ": ");
		// Checking if someone is talking to the bot through query
		int index2 = line.find("PRIVMSG " + user_ + " :");
		if (index != std::string::npos) // If someone talked to us on the channel
		{
			// Okay, someone is talking to us
			// Who is talking to us?
			std::string userWhoTalkedToUs = line.substr(1, line.find("!") - 1);
			// What did they say?
			index += 8 + channel_.size() + 2 + user_.size() + 2; // Finding the index after "iana: "
			if (line.size() <= index) // Making sure that there is some text after the "iana: "
				return false;
			std::string userSaidToUs = line.substr(index);
			
			// Cleaning the line, trim, lower
			userSaidToUs = cleanLine(userSaidToUs);
						
			std::cout << "Lowercase: \"" << userSaidToUs << "\"" << std::endl;
			
			// Now, let's find out what they want
			if (lotteryNumbers(userSaidToUs)) return true;
		}
		else if (index2 != std::string::npos) // If someone talked to us through query
		{
			// Okay, someone is talking to us
			// Who is talking to us?
			std::string userWhoTalkedToUs = line.substr(1, line.find("!") - 1);
			std::string userAddressWhoTalkedToUs = line.substr(1, line.find(" ") - 1);
			// What did they say?
			index2 += 8 + user_.size() + 2;
			if (line.size() <= index2) // Making sure there is some text after the "iana :"
				return false;
			
			std::string userSaidToUs = line.substr(index2);
			
			// Cleaning line
			userSaidToUs = cleanLine(userSaidToUs);
			
			// Let's find out what they want
			if (MasterCalling(userSaidToUs, userAddressWhoTalkedToUs)) return true; // If master is taking command
		}
		return false;
	}
	
	// Master is telling who is, so we from whom to take commands from
	bool IrcBot::MasterCalling(std::string line, std::string userName)
	{
		// Getting just the nick from MasterName
		std::string userNick = userName.substr(0, userName.find("!"));
		if (line == MasterPassword_)
		{
			// The password was correct, checking if we are allowed to change Master
			if (MasterChangable_ == true)
			{
				Master_ = userName;
				sayToUser("Master", userNick);
			}
		}
		else if (line == "who is thy master?")
		{
			if (Master_ == "")
				sayToUser("I have none", userNick);
			else
				sayToUser(Master_, userNick);
		}
		else if(line == "do a barrel roll")
		{
			if (userName == Master_)
				sayToUser("WEEEEEE", userNick);
			else
				sayToUser("Nope", userNick);
		}
		
		// If master is compromised
		if (line == "disable master")
		{
			MasterChangable_ = false;
			MasterIsLaw_ = false;
			userDisabledMaster_ = userName;
			sayToUser("Done", userNick);
		}
		
		// If master tells us to do something. Only if master is not compromised
		if (MasterIsLaw_ == true && userName == Master_)
		{
			int kickIndex = line.find("kick user ");
			std::cout << "KickIndex: " << kickIndex << std::endl;
			if (kickIndex == 0)
			{
				std::vector<std::string>kickLine = str2vec(line, ' ');
				std::cout << "KickLine size: " << kickLine.size() << std::endl;
				if (kickLine.size() >= 3) //There has to be atleast 3 words in the kick order. E.g. "kick user samiy apple homo"
				{
					// Kicking user from the channel
					std::string userToKick = kickLine.at(2);
					std::string reason = "";
					for (int i = 3; i < kickLine.size(); ++i)
					{
						reason += kickLine.at(i) + " ";
					}
					// Kicking user, if operator
					if (operator_ == true)
					{
						if (!kickUser(userToKick, reason))
						{
							sayToUser("Can't kick", userNick);
						}
					}
					else
						sayToUser("I don't have operator status", userNick);
				}
			}
			int opIndex = line.find("give me op");
			if (opIndex == 0)
			{
				// Giving master op to the channel if we are op
				if (operator_ == true)
				{
					// :Q!TheQBot@CServe.quakenet.org MODE #08I220C +o Foranamo
					write("MODE " + channel_ + " +0 " + userNick);
					sayToUser("Done", userNick);
				}
				else // We are not op
				{
					sayToUser("I'm not op :(", userNick);
				}
			}
			int opIndexForce = line.find("give me op!");
			if (opIndexForce == 0)
			{
				// Trying to give op
				write("MODE " + channel_ + " +o " + userNick);
				sayToUser("Done", userNick);
			}
			
			// Remove op from user
			int removeOp = line.find("remove op from ");
			if (removeOp == 0)
			{
				std::vector<std::string>removeOpCommands = str2vec(line, ' ');
				// There has to be atleast 4 parts in the command, E.g. "remove op from samiy";
				if (removeOpCommands.size() >= 4)
				{
					std::string userToRemoveOpFrom = removeOpCommands.at(3);
					write("MODE " + channel_ + " -o " + userToRemoveOpFrom);
					sayToUser("Done", userNick);
				}
			}
			
			int sayIndex = line.find("say");
			if (opIndex == 0)
			{
				std::string sayText = line.substr(4);
				sayToChannel(sayText);
			}
		}
		/*
		else
		{
			sayToUser("I'm sorry Dave, I'm afraid I can't do that", userNick);
		}
		*/
	}
	
	std::vector<int> IrcBot::newLotteryNumbers()
	{
		// Scrambeling the random generator
		srand(time(NULL)); 
		std::vector<int>returnValue;
		while (returnValue.size() < 7)
		{
			int number = rand() % 39 + 1; // 1 - 39;
			
			// Checking if the number is already in the list
			if (std::find(returnValue.begin(), returnValue.end(), number) == returnValue.end())
			{
				// The number isn't in the vector, adding it
				returnValue.push_back(number);
			}
			else
			{
				// Number is already in the vector
				// Theoretically, the random could forever give us a number that is in the vector
				// Instead, we will either add or remove one number from
				int plusOrMinus = rand() % 2; // 0 substracts, 1 adds
				int changedNumber = number;
				if (plusOrMinus == 0)
				{
					while (1)
					{
						changedNumber -= 1;
						if (changedNumber <= 0) // If we go too low, go to max
							changedNumber = 39;
						if (std::find(returnValue.begin(), returnValue.end(), changedNumber) == returnValue.end())
						{
							// The number isn't in the vector, adding it
							returnValue.push_back(changedNumber);
							break;
						}
					}
				}
				else // Add
				{
					while (1)
					{
						changedNumber += 1;
						if (changedNumber > 39) // If we go too high, go to min
							changedNumber = 1;
						if (std::find(returnValue.begin(), returnValue.end(), changedNumber) == returnValue.end())
						{
							// The number isn't in the vector, adding it
							returnValue.push_back(changedNumber);
							break;
						}
					}
				}
			}
		}
		
		// Sorting the returnValue
		std::sort(returnValue.begin(), returnValue.end());
		
		return returnValue;
	}
	
	bool IrcBot::lotteryNumbers(std::string userLine)
	{
		if (userLine == "lotto" 
		|| userLine == "lottery" 
		|| userLine == "lottonumerot"
		|| userLine == "lotterynumbers")
		{
			std::cout << "LOTTO SAID" << std::endl;
			// Let's check if lottery number need updating
			// i.e. if the lotterynumbers was from last week, then we need to get new numbers
			std::string lottoTime = botDB_.getLottoUpdateTime();
			
			// Lottery is done every saturday (weekday 6)
			if (hasLotteryPassed(lottoTime))
			{
				// We need to create new lottery numbers
				std::vector<int>newNumbers = newLotteryNumbers();
				
				// Putting the new numbers to the database
				botDB_.setLottoNumbers(newNumbers);
			}
			// Retrieve lottery numbers from the database
			std::vector<int>lottoNr = botDB_.getLottoNumbers();
			
			// Arranging numbers, just in case
			std::sort(lottoNr.begin(), lottoNr.end());
			
			// Creating a string of what we want to say
			std::string sayLotto = "";
			for (int i = 0; i < lottoNr.size(); ++i)
			{
				sayLotto += convertInt(lottoNr.at(i));
				if (i != 6)
					sayLotto += ", ";
			}
			
			sayToChannel(sayLotto);
			
			return true;
		}
		return false;
	}
	
	bool IrcBot::hasLotteryPassed(std::string lottoTime)
	{
		// When will the lottery be drawn?
		
		struct tm lTime;
		struct tm *lotteryDate;
		struct tm lDateTime;
		// initialize declared structs
		memset(&lTime, 0, sizeof(struct tm));
		
		// Converting database lottotime string to tm
		strptime(lottoTime.c_str(), "%Y-%m-%d %H:%M:%S", &lTime); 
		//database lotto time tm to time_t
		time_t lT = mktime(&lTime); 
		
		// The database lotto times weekday (sunday = 0)
		int lottoWeekDay = getWeekDay(lottoTime);
		
		// How many days are there left until the lottery is drawn, counted from the database time
		int daysLeftToLotto = 6 - lottoWeekDay;
		
		// How many seconds left to saturday (from midnight to midnight) (database time)
		time_t lottoDrawDate = lT + (time_t)daysLeftToLotto * 24 * 60 * 60; 
		
		// If the lottoTime is saturday, then we just need to check if it is after 20:45:00
		if (daysLeftToLotto == 0) // The lotterynumbers were done on a saturday
		{
			int lottoClockTime = 20 * 60 * 60 + 45 * 60; // 20:45 turned into seconds
			int lTimeClockTime = lTime.tm_hour * 60 * 60 + lTime.tm_min * 60 + lTime.tm_sec; // The time that the lotto numbers were drawn
			
			if (lTimeClockTime > lottoClockTime) // If the lotterynumbers in the database is after 20:45
			{
				// The lottoDrawDate has to be moved forward with one week (7 days)
				lottoDrawDate += 7 * 24 * 60 * 60;
			}
		}
		
		lotteryDate = gmtime(&lottoDrawDate); // Date and time of when next lottery will be (tm)
		std::string lotteryDateTimeStr = convertInt(lotteryDate->tm_year + 1900)
										+ "-" + convertInt(lotteryDate->tm_mon + 1)
										+ "-" + convertInt(lotteryDate->tm_mday)
										+ " 20"
										+ ":45"
										+ ":00";
		strptime(lotteryDateTimeStr.c_str(), "%Y-%m-%d %H:%M:%S", &lDateTime);
		
		
		// Now we have in lDateTime (tm) the date and time when the lottery numbers will be drawn for the
		// numbers that are right now in the database.
		// Now we check if the lottery drawing day has passed. If it has passed, we will return true.
		
		// Converting lDateTime to time_t
		time_t lDateTimeInSeconds = mktime(&lDateTime);
		// Time right now
		time_t currentTimeInSeconds = time(NULL);
		
		if (lDateTimeInSeconds < currentTimeInSeconds) // If the lottery drawing date has passed.
			return true;
		
		return false;
	}
	
	bool IrcBot::hello()
	{
		// Checking if someone said hello to the bot
		std::cout << "HELLO CHECKER: " << ircText_ << std::endl;
		if (ircText_.find(channel_ + " :" + user_ + ": " + "hello") != -1)
		{
			std::string user = ircText_.substr(1, ircText_.find("!") - 1);
			sayToChannel("Hi!", user);
			
			return true;
		}
		return false;
	}
	
	bool IrcBot::askingForGame(std::string line, std::string game, std::vector<std::string>players)
	{
		// Checking if someone said the game word
		if (game != getLineThatUserSaid(line))
			return false;
		
		// The users on the channel can be found from the privates (names_)
		// Will shout to them who play the game
		std::string say = "";
		std::vector<std::string>playersOnline;

		for (int j = 0; j < players.size(); ++j)
		{
			for (int i = 0; i < names_.size(); ++i)
			{
				if (str2lower(names_.at(i)) == players.at(j))
				{
					playersOnline.push_back(names_.at(i));
					break;
				}
			}
		}
		
		if (playersOnline.size() <= 0)
			return false;
		else
		{
			// Checking if it has been less than 15 minutes since we previously shouted for a game
			time_t tempTime = time(0); // Current time
			time_t fifteenMins = 60 * 15;
			if (tempTime < gameShoutTime + fifteenMins) // If has been less than 15 mins
				return false;
		}
		
		// Putting all player names to string
		for (int i = 0; i < playersOnline.size(); ++i)
		{
			say += playersOnline.at(i) + " ";
		}
		say += game;
		
		sayToChannel(say);
		
		gameShoutTime = time(0); // Updating the time when we previosly shouted for a game
		
		return true;
	}
	
	bool IrcBot::l4d(std::string line)
	{
		std::vector<std::string>players;
		players.push_back("ratamo");
		players.push_back("ratamoh");
		players.push_back("foranamo");
		players.push_back("dylle");
		players.push_back("dylle_");
		players.push_back("erlis");
		players.push_back("kristuz");
		players.push_back("lepya");
		players.push_back("mantsa");
		players.push_back("samiy");
		players.push_back("soppa--");
		players.push_back("soppa");
		players.push_back("unas");
		players.push_back("unas_");
		
		return askingForGame(line, "l4d", players);
	}
	
	// Returns true if a user asked for his/her karma points
	bool IrcBot::karma(std::string line)
	{
		std::string writtenText = getLineThatUserSaid(line);
		writtenText = trim(writtenText); // Trimming text, just in case
		if (str2lower(writtenText) != "!karma")
		{
			//std::cout << "User didn't say !karma, they said (lowered): " << str2lower(writtenText) << std::endl;
			return false;
		}
		// User said !karma
		else
		{
			std::string username = getUserNameFromLine(line);
			std::string karmaCount = botDB_.getUserKarmaCount(username);
			
			// Telling the user his/her karmapoints
			sayToChannel("Karma for " + username + ": " + karmaCount);
		}
		return true;
	}

	// Returns true if a user asked for his/her wanha points
	bool IrcBot::wanha(std::string line)
	{
		std::string writtenText = getLineThatUserSaid(line);
		writtenText = trim(writtenText); // Trimming text, just in case
		
		if (str2lower(writtenText) != "!wanha")
		{
			//std::cout << "User didn't say !karma, they said (lowered): " << str2lower(writtenText) << std::endl;
			return false;
		}
		// User said !karma
		else
		{
			std::string username = getUserNameFromLine(line);
			std::string wanhaCount = botDB_.getUserWanhaCount(username);

			// Telling the user his/her karmapoints
			sayToChannel("Wanhapoints for " + username + ": " + wanhaCount);
		}
		return true;
	}

	bool IrcBot::cs(std::string line)
	{	
		std::vector<std::string>players;
		players.push_back("ratamo");
		players.push_back("ratamoh");
		players.push_back("foranamo");
		players.push_back("erlis");
		players.push_back("filsu");
		players.push_back("filsuu");
		players.push_back("kristuz");
		players.push_back("lepya");
		players.push_back("mantsa");
		players.push_back("samiy");
		players.push_back("soppa--");
		players.push_back("soppa");
		players.push_back("unas");
		players.push_back("unas_");
		
		return askingForGame(line, "cs", players);
	}
	
	void IrcBot::shouldWeKickSomeone()
	{
		// If we don't have operator status, then no point in continuing
		if (!operator_)
			return;
		
		// Users that should be kicked are stored in here.
		std::vector<std::string> users = botDB_.hasSomeoneKickedUsInThePast();
		
		if (users.size() > 0)
		{
			// We need to have our revenge
			for (int i = 0; i < users.size(); ++i)
			{
				std::cout << "KICKING USER: " << users.at(i) << std::endl;
				if (kickUser(users.at(i), "Revenge!"))
					botDB_.resetKickStatus(users.at(i)); // Removing from the database that the user should be kicked
			}
		}
	}
	
	bool IrcBot::weGotOperatorStatus(std::string line)
	{
		// Checking if we got operator status		
		int mode = line.find(" MODE " + channel_ + " ");
		if (mode == -1)
			return false;
		int addOp = line.find(" +o", mode);
		if (addOp == -1)
			return false;
		int me = line.find(user_, addOp);
		if (me == -1)
			return false;
		
		// Apparently we were given op, checking if we need to revenge someone for kicking us
		std::vector<std::string> users = botDB_.hasSomeoneKickedUsInThePast();
		
		if (users.size() > 0)
		{
			// We need to have our revenge
			for (int i = 0; i < users.size(); ++i)
			{
				std::cout << "KICKING USER: " << users.at(i) << std::endl;
				if (kickUser(users.at(i), "Revenge!"))
					botDB_.resetKickStatus(users.at(i));
			}
		}
		
		operator_ = true;
		return true;
		
	}
	bool IrcBot::weLostOperatorStatus(std::string line)
	{
		// Checking if we lost our operator status		
		int mode = line.find(" MODE " + channel_ + " ");
		if (mode == -1)
			return false;
		int addOp = line.find(" -o", mode);
		if (addOp == -1)
			return false;
		int me = line.find(user_, addOp);
		if (me == -1)
			return false;
		
		std::string user = getUser(line);
		
		sayToChannel(":(", user);
		
		operator_ = false;
		return true;
		
	}
	bool IrcBot::wasIKicked(std::string line)
	{
		// Did they kick you :'(. Well let's get back there and write down who kicked us 
		// and kick them the second we get the operator status
		int found = line.find("KICK " + channel_ + " " + user_);
		if (found == -1)
			return false;
		
		// Seems like we were kicked.... Shit!
		// Remembering who kicked us
		std::string kickedBy = getUser(line);
		
		// The user is marked in the database as kicked us atleast once
		botDB_.addKickedUs(kickedBy);
		
		// Getting back to the channel
		joinChannel(channel_, false);
		
		/*
		int kickAmount = botDB_.getUserKickAmount(kickedBy);
		std::string say = "";
		if (kickAmount == 1) // If this was the first time being kicked by this user
			say = "You kicked me? How about making it a fair fight? Op for me?";
		else
			say = "Fuck you...";
		sayToChannel(say, kickedBy);
		*/
		
		return true;
	}
	bool IrcBot::otherBotTalking(std::string line)
	{
		std::string user = getUser(line);
		if (user == "")
			return false;
		
		if (user == "TiTeLAN" || user == "vassykka")
		{
			std::cout << "SKIPPING USER: " << user << std::endl;
			return true;
		}
		
		return false;
	}
	
	bool IrcBot::isYoutubeLink(std::string link)
	{
		// Checking if the given link is from youtube
		// If starts with http://www.youtube.com/
		if (link.find("http://www.youtube.com") == 0)
			return true;
		if (link.find("http://youtu.be") == 0)
			return true;
	}
	
	std::string IrcBot::convertYoutubeLink(std::string link)
	{
		// Getting the video ID (v=EistK5DX8ug)
		std::string vID = "";
		if (link.find("http://youtu.be") == 0)
		{
			// The ID will come right after the /, unless http://youtu.be/ is given
			if (link.size() < 17)
			{
				// Returning youtube.com because only youtube.be is given, no video link
				return "http://www.youtube.com";
			}
			vID = link.substr(16);
			// Checking if something like ?t=5s is given after
			int qPos = vID.find("?");
			if (qPos != std::string::npos)
			{
				// Leaving only the ID
				vID = vID.substr(0, qPos);
			}
		}
		else
		{
			// The ID on youtube.com is after v=
			int vPos = link.find("v=");
			if (vPos == std::string::npos)
			{
				// v= wasn't found, return url as it was
				return link;
			}
			// Making sure that there is something after the v=
			if (link.size() < (vPos + 3))
				return "http://www.youtube.com";
			vID = link.substr(vPos + 2);
			// removing extra stuff after the ID
			int andPos = vID.find("&");
			if (andPos != std::string::npos)
			{
				// Removing everything after the &
				vID = vID.substr(0, andPos);
			}
		}
		// Now that we have the video ID, we can build the youtube.com link
		std::string output = "";
		output = "http://www.youtube.com/watch?v=" + vID;
		return output;
	}
	
	bool IrcBot::linkPasted(std::string line)
	{		
		// If it is a PRIVMSG
		int index = line.find(" PRIVMSG " + channel_ + " :");
		if (index != -1)
		{
			// Okay, it was a message, but does it have a link in it?
			int urlStartIndex = line.find("http://");
			if (urlStartIndex == -1)
			{
				urlStartIndex = line.find("www.");
				if (urlStartIndex == -1)
					return false;
			}
			
			// Find the first space after the link, if there is none, then there is nothing after the link
			int urlEndIndex = line.find(' ', urlStartIndex);
			
			std::string url = "";
			// If the space was found, then we know that the link ends there
			if (urlEndIndex != -1)
				url = line.substr(urlStartIndex, (urlEndIndex - urlStartIndex) + 1);
			else // There is no space after the link, so the link is the last thing on the line
				url = line.substr(urlStartIndex);
			
			// Someone might have put something after the link that is not allowed, like , or .
			// We will remove those
			for (int i = url.size() - 1; i > 0; --i)
			{
				if (url.at(i) == ',' || url.at(i) == '.' || url.at(i) == ':' || url.at(i) == ';' || 
					url.at(i) == '"' || url.at(i) == '(' || url.at(i) == ')' || url.at(i) == '\\' ||
					url.at(i) == '>' || url.at(i) == '<' || url.at(i) == ' ' || url.at(i) == '\n' ||
					url.at(i) == '\r' || url.at(i) == '/')
				{
					url.erase(i, 1); // Remove the last char
				}
				else
				{
					break;
				}
			}
			
			// Now, we have to add the http:// to the beginning (if needed)
			if (url.find("http://") != 0)
				url = "http://" + url;
				
			// Making sure that the link is atleast 10 chars long
			if (url.size() < 12)
				return false;
			
			std::cout << "LINK THAT WAS PASTED: \"" << url << "\"" << std::endl;
			
			// Now that we have the link, we need to know who pasted the link
			std::string user = getUserNameFromLine(line);
			if (user.length() == 0)
			{
				return false;
			}
			/*
			int usernameEnd = line.find('!');
			if (usernameEnd == -1)
				return false;
			std::string user = line.substr(1, usernameEnd - 1);
			*/

			
			std::cout << "USER WHO PASTED THE LINK: \"" << user << "\"" << std::endl;
			
			// Check if the link is from Youtube
			if (isYoutubeLink(url))
			{
				std::cout << "Youtube link given: \"" << url << "\"" << std::endl;
				// It is from youtube, converting the link to common youtube format
				// i.e. http://www.youtube.com/watch?v=EistK5DX8ug
				url = convertYoutubeLink(url);
				std::cout << "Converted link: \"" << url << "\"" << std::endl;
			}
			
			// Okay, lets check if the link is old and stuff
			bool returnValue = false;
			if (!botDB_.isLinkOld(url))
			{
				std::cout << "LINK IS NEW" << std::endl;
				botDB_.addNewLink(user, url);
			}
			else // Link is old
			{
				// Getting information about the user and the originality
				std::string originalUser = botDB_.getOriginalUser(url);
				std::string originalDate = botDB_.getOriginalDate(url);
				
				// Users are allowed to paste their own links
				if (user != originalUser)
				{
					botDB_.addUserOldCount(user);
					
					// Okay, and old link. Changing karma points
					botDB_.addKarmaPoints(originalUser, true);
					botDB_.addKarmaPoints(user, false);
					
					std::string userWanhaCount = botDB_.getUserWanhaCount(user);
					std::string userKarmaCount = botDB_.getUserKarmaCount(user);
					std::string originalUserKarmaCount = botDB_.getUserKarmaCount(originalUser);
					
					std::string say = "WANHA! ";
					// Checking the user was the same one that originally pasted the link
					say += "Originally linked by: " + originalUser + 
					" (Karma: " + originalUserKarmaCount + ") ";
					say += "on " + originalDate + ". ";
					
					say += "You have now linked " + userWanhaCount + " old link(s). " + 
					"Karma: " + userKarmaCount;
					sayToChannel(say, user);
				}
			}
			
			// Telling the users on the channel, what the topic is of this page
			std::string title = getPageTitle(url);
			if (title != "")
				sayToChannel("Title: " + title);
			
			// A link was pasted, so we are returning true
			return true;
		}
		return false;
	}
	
	std::string IrcBot::getPageTitle(std::string url)
	{
		// -O Outputs the file
		// -F Forces the file to be html. If it is something else, then nothing is outputted
		// --delete-after Deletes files after download
		std::string command = "wget --quiet -F --delete-after -O - " + url;
		FILE *fp=popen(command.c_str(), "r");
		const int size = 100000;
		char buffer[size] = {'\0'};

		// Putting lines to vector
		std::string temp = "";
		std::vector<std::string> html;
		int counter = 0;
		while (!feof(fp))
		{
			if (fgets (buffer, size, fp) != NULL)
			{
				for (int i = 0; i < size; ++i)
				{
					// End of buffer
					if (buffer[i] == '\0')
					{
						if (temp != "")
							html.push_back(temp);
						break;
					}
				
					if ((buffer[i] == '\r' || buffer[i] == '\n') && temp != "")
					{
						// End of line. Putting the string that we got so far to the vector
						html.push_back(temp);
						temp = "";
					}
					else if (buffer[i] != '\r' && buffer[i] != '\n')
					{
						// Putting char to string
						temp += buffer[i];
					}
				}
			}
			++counter;
			// This is for quitting if the file is too large
			// E.g. if the file is a picture, then a segmentation fault will occure before
			// the end, this will prevent that.
			if (counter > 50) 
				break;
		}
		pclose(fp); //close pipe
		// Now that we have the html source in the vector, we will search for the title from it
		for (int i = 0; i < html.size(); ++i)
		{
			int index = html.at(i).find("<title>");
			if (index != std::string::npos)
			{
				// Looking for the end of the title "</title>"
				for (int j = i; j < html.size(); ++j)
				{
					int index2 = html.at(j).find("</title>");
					if (index2 != std::string::npos)
					{
						// We have the start and end line of the title
						// Putting lines to one
						std::string titleLine = "";
						for (int k = i; k <= j; ++k)
						{
							std::string temp = html.at(k);
							// Removing \n and \r
							while (temp.at(temp.size() - 1) == '\n'
									|| temp.at(temp.size() - 1) == '\r')
							{
								temp.erase(temp.size() - 1);
							}
							titleLine += temp + " ";
						}
						// Removing <title>
						index = titleLine.find("<title>");
						//titleLine = trim(titleLine);
						if ((index + 7) > titleLine.size()) // If there is nothing after <title>
							return "";
						titleLine = titleLine.substr(index + 7);
						
						// Removing </title>
						index2 = titleLine.find("</title>");
						//titleLine = trim(titleLine);
						if (index2 == std::string::npos) // No title found, atleast nothing good
							return "";
						titleLine = titleLine.substr(0, index2);
						
						// Trimming answer
						titleLine = trim(titleLine);
						return titleLine;
					}
				}
				// no </title> was found, returning nothing
				return "";
			}
		}
		return "";
	}
	
	bool IrcBot::userLeft(std::string line)
	{
		int foundIndex = line.find("PART " + channel_);
		if (foundIndex != -1) // If we found what we were looking for
		{
			// Okay, someone left the channel, now we need to find out who
			// Taking the name between : and !, e.g. :roger2!~roger@a91-155-189-191.elisa-laajakaista.fi PART #iana
			std::string partUser = line.substr(1, line.find('!') - 1);
			
			if (partUser.size() != 0)
			{
				// Now we just need to remove the user from the list
				return removeUser(partUser);
			}
		}
		// We didn't find what we were looking for
		return false;
	}
	bool IrcBot::userJoined(std::string line)
	{
		int foundIndex = line.find("JOIN " + channel_);
		if (foundIndex != -1) // If we found what we were looking for
		{
			// Okay, someone joined the channel, now we need to find out who
			// Taking the name between : and !, e.g. :roger2!~roger@a91-155-189-191.elisa-laajakaista.fi JOIN #iana
			std::string joinUser = line.substr(1, line.find('!') - 1);
			
			if (joinUser.size() != 0)
			{
				// Now we just need to add the user to the list
				addUser(joinUser);
				
				// Do we need need to kick this guy? (Revenge)
				if (operator_ == true)
				{
					// Apparently we are op, checking if we need to revenge the guy who came online
					std::vector<std::string> users = botDB_.hasSomeoneKickedUsInThePast();
					
					if (users.size() > 0)
					{
						// We need to have our revenge
						for (int i = 0; i < users.size(); ++i)
						{
							std::cout << "KICKING USER: " << users.at(i) << std::endl;
							if (kickUser(users.at(i), "Revenge!"))
								botDB_.resetKickStatus(users.at(i));
						}
					}
				}
				
				return true;
			}
		}
		return false;
	}
	
	bool IrcBot::userChangedNick(std::string line)
	{
		int foundIndex = line.find("NICK :");
		if (foundIndex != -1) // If we found what we were looking for
		{
			// Okay, someone changed their nick
			// Taking the name between : and !, e.g. :roger2!~roger@a91-155-189-191.elisa-laajakaista.fi NICK :roger3
			std::string nickUserOld = line.substr(1, line.find('!') - 1);
			std::string nickUserNew = line.substr(foundIndex + 6);
			
			if (nickUserNew.size() != 0 && nickUserOld.size() != 0)
			{
				// Now we just need to change the nickname from the list
				return changeNick(nickUserOld, nickUserNew);
			}
		}
		return false;
	}

	std::string IrcBot::getUserNameFromLine(std::string line)
	{
		// Now that we have the link, we need to know who pasted the link
		int usernameEnd = line.find('!');
		if (usernameEnd == -1)
		{
			// If there wasn't a "!" in the line, then something went wrong and we can't determine the username
			return "";
		}
		return line.substr(1, usernameEnd - 1);
	}
}