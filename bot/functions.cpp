// Has some useful functions
#include <algorithm>
#include <arpa/inet.h>
#include <cstdlib>
#include <ctime>
#include <errno.h>
#include <fcntl.h>
#include <iostream>
#include <netdb.h>
#include <netinet/in.h>
#include <signal.h>
#include <sstream>
#include <stdlib.h>
#include <string>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>
#include <vector>

namespace IB
{
	#include "functions.h"

	std::vector<std::string>str2vec(std::string str, char delimiter)
	{
		// The vector where the lines will be stored and then returned
		std::vector<std::string>returnValue;
		
		while (true)
		{
			// Find the first index with a newline
			int newlineIndex = str.find(delimiter);
			if (newlineIndex == -1)
			{
				// This is the last line
				returnValue.push_back(str);
				break;
			}
			// Taking every char from the beginning, all the way to the newline
			returnValue.push_back(str.substr(0, newlineIndex));
			
			// Removing the first line, including the newline
			if (str.size() > newlineIndex + 1) // Making sure there is still something left after the newline
			{
				str = str.substr(newlineIndex + 1);
			}
			else
				break;
		}
		
		return returnValue;
	}
	
	std::string convertInt(int number)
	{
	   std::stringstream ss;//create a stringstream
	   ss << number;//add number to the stream
	   return ss.str();//return a string with the contents of the stream
	}
	
	// Tries to find the words in the vector from the line. If one or more is found, returns true
	std::string findWordsInLine(std::string line, std::vector<std::string>words)
	{
		for (int i = 0; i < words.size(); ++i)
		{
			// Finding the word from the line. We are not going to just simply use the find
			// because the word can be inside a another word. We will make sure that the word has
			// spaces around it, dots, commas, excalamtion marks, question marks or nothing.
			int index = line.find(words.at(i));
			if (index != std::string::npos) // if a word was found
			{
				// These booleans tell us if the start on end of word is ok
				// e.g. word "hand", line has "handball", start is ok but end is not
				bool startOK = false;
				bool endOK = false;
				
				// Start
				if (index == 0 || // If is the first word
					charIsSpaceOrSpecial(line.at(index - 1))) // if before the word was a special char
				{
					startOK = true;
				}
				
				// End
				if ((index + words.at(i).size()) == line.size() || // If word is the last in the line
					charIsSpaceOrSpecial(line.at(index + words.at(i).size()))) // if special char after word
				{
					endOK = true;
				}
				
				if (startOK && endOK)
					return words.at(i);
			}
		}
		
		// Nothing found, returning false
		return "";
	}
	
	bool charIsSpaceOrSpecial(char character)
	{
		const int size = 22;
		char characters[size] = {' ', ',', '.', ';', ':', '-', '_', '!', '"', '#', '�', '%', '&',
								'/', '(', ')', '=', '?', '�', '*', '\n', '\r'};
		for (int i = 0; i < size; ++i)
		{
			if (characters[i] == character)
				return true;
		}
		return false;
	}
	
	std::string str2lower(std::string str)
	{
		//std::transform(userSaidToUs.begin(), userSaidToUs.end(), userSaidToUs.begin(), ::tolower);
		std::transform(str.begin(), str.end(), str.begin(), ::tolower);
		return str;
	}
	
	
	std::string trim(std::string str)
	{
		// Removing space from beginning of string
		while (str.size() > 0)
		{
			if(str.at(0) == ' ')
				str = str.substr(1);
			else
				break;
		}
		// Removing the space from the end
		while (str.size() > 0)
		{
			if(str.at(str.size() - 1) == ' ')
				str = str.substr(0, str.size() - 2);
			else
				break;
		}
		
		return str;
	}
	
	// Converts a vector<string> to vector<int>
	std::vector<int>strVec2IntVec(std::vector<std::string> str)
	{
		std::vector<int>returnValue;
		for (int i = 0; i < str.size(); ++i)
		{
			int temp = atoi(str.at(i).c_str());
			returnValue.push_back(temp);
		}
		return returnValue;
	}
	
	int getDatePart(datePart part, std::string date)
	{
		// converting "2013-01-09 17:57:34" to "2013-01-09-17-57-34"
		std::replace(date.begin(), date.end(), ' ', '-');
		std::replace(date.begin(), date.end(), ':', '-');
		
		std::vector<std::string>parts = str2vec(date, '-');
		std::vector<int>intParts = strVec2IntVec(parts);
		
		if (intParts.size() != 6)
		{
			std::cout << "The splitting of the date failed: " << intParts.size() << " " << date << std::endl;
			return -1;
		}
		if (part == YEAR)
			return intParts.at(0);
		if (part == MONTH)
			return intParts.at(1);
		if (part == DAY)
			return intParts.at(2);
		if (part == HOUR)
			return intParts.at(3);
		if (part == MINUTE)
			return intParts.at(4);
		if (part == SECOND)
			return intParts.at(5);
	}
	
	int getWeekDay(std::string date)
	{
		int year = getDatePart(YEAR, date);
		int month = getDatePart(MONTH, date);
		int day = getDatePart(DAY, date);
		int hour = getDatePart(HOUR, date);
		int minute = getDatePart(MINUTE, date);
		int second = getDatePart(SECOND, date);
			
		// Getting the weekday when lotterynumber was created. Day is 1-based, month is 0 based. Years since 1900
		std::tm time = {second, minute, hour, day, (month - 1), (year - 1900) };
		std::time_t timeConv = std::mktime(& time);
		std::tm const *time_out = std::localtime(& timeConv);
		int weekday = time_out->tm_wday; // Sunday = 0, monday = 1 etc.
		
		return weekday;
	}
	
	std::string getCurrentTime()
	{
		std::time_t t = time(0); // current time
		struct tm * now = localtime(& t);
		
		std::string returnValue = "";
		std::string temp = "";
		
		// year
		returnValue = convertInt(now->tm_year + 1900);
		
		// month, 0-based
		if (now->tm_mon < 10)
			temp = "0" + convertInt(now->tm_mon + 1);
		else
			temp += convertInt(now->tm_mon + 1);
		returnValue += "-" + temp;
		
		// day
		if (now->tm_mday < 10)
			temp = "0" + convertInt(now->tm_mday);
		else
			temp = convertInt(now->tm_mday);
		returnValue += "-" + temp;
		
		// hour
		if (now->tm_hour < 10)
			temp = "0" + convertInt(now->tm_hour);
		else
			temp = convertInt(now->tm_hour);
		returnValue += " " + temp;
		
		// minute
		if (now->tm_min < 10)
			temp = "0" + convertInt(now->tm_min);
		else
			temp = convertInt(now->tm_min);
		returnValue += ":" + temp;
		
		// second
		if (now->tm_sec < 10)
			temp = "0" + convertInt(now->tm_sec);
		else
			temp = convertInt(now->tm_sec);
		returnValue += ":" + temp;
		
		return returnValue;
	}
	
	// Remove all characters from the left and right of the string
	void trim(std::string &str, char chr)
	{
		while(1)
		{
			if (str.size() == 0)
				break;
			// First the beginning
			if (str.at(0) == chr)
			{
				str.erase(0, 1);
			}
			// Then the end
			else if (str.at(str.size() - 1) == chr)
			{
				//str = str.substr(0, str.size() - 1);
				str.erase(str.size() - 1, 1);
			}
			else
				break;
		}
	}
	
	// Cleans a line, trims and stuff
	std::string cleanLine(std::string line)
	{
		line = str2lower(line);
		
		// Trimmin line
		trim(line, ' ');
		trim(line, '\n');
		trim(line, '\r');
		trim(line, '\t');
		trim(line, ' ');
		
		return line;
	}
}




























