#ifndef BOTBASE_H
#define BOTBASE_H

#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <time.h>
#include <cstdlib>
#include <vector>
#include <sqlite3.h>
//#include "functions.h"

namespace IB
{
	class BotBase
	{
		public:
			BotBase();
			BotBase(std::string file);
			~BotBase();
			
			// database methods
			bool isLinkOld(std::string url);
			bool addNewLink(std::string user, std::string url);
			bool addNewLink(std::string user, std::string url, std::string datetime);
			bool addUserOldCount(std::string user);
			bool addKarmaPoints(std::string user, bool addPoints);
			bool addKickedUs(std::string user);
			std::string getOriginalUser(std::string url);
			std::string getOriginalDate(std::string url);
			std::string getUserWanhaCount(std::string user);
			std::string getUserKarmaCount(std::string user);
			std::vector<std::string>hasSomeoneKickedUsInThePast();
			void resetKickStatus(std::string user);
			int getUserKickAmount(std::string user);
			
			// Apple
			//std::vector<std::string> getForbiddenWords();
			//int addUserStrike(std::string user, std::string word);
			//void removeUserStrikes(std::string user);
			
			// Lotto
			std::string getLottoUpdateTime();
			std::vector<int>getLottoNumbers();
			bool setLottoNumbers(std::vector<int>numbers);
			
		private:
			bool doesUserExist(std::string user);
			void createUser(std::string user);
			
			// Apple
			//bool hasUserSaidWordBefore(std::string user, std::string word);
			//void createNewUserSaidWord(std::string user, std::string word);
			//void addWordCountForUser(std::string user, std::string word);
			//void addStrikesForUser(std::string user);
			//int getUserStrikeAmount(std::string user);
			//int getForbiddenWordID(std::string word);
			int getUserID(std::string user);
		
			std::string dbfile_;
			sqlite3 *db_;
			sqlite3_stmt *stmt_;
			char *szErrMsg_;
			// This holds the values that sqlite3 returned.
			std::vector < std::vector < std::string > > sqlResult_;
			
			// SQL methods
			bool openDatabase();
			void runSqlCommand(std::string sqlCommand); // Runs the command and stores result to sqlResult_
			std::string getSqlResultText(); // Checks that there is a value in the sqlResults (correctly) and returns it
	};
}

#endif