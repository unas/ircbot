#include <algorithm>
#include <arpa/inet.h>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <errno.h>
#include <fcntl.h>
#include <iostream>
#include <netdb.h>
#include <netinet/in.h>
#include <signal.h>
#include <sstream>
#include <stdio.h>  
#include <stdlib.h>
#include <string>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>
#include <vector>

using namespace std;

vector<int>newLotteryNumbers()
{
	// Scrambeling the random generator
	srand(time(NULL)); 
	vector<int>returnValue;
	while (returnValue.size() < 7)
	{
		int number = rand() % 39 + 1; // 1 - 39;
		
		// Checking if the number is already in the list
		if (find(returnValue.begin(), returnValue.end(), number) == returnValue.end())
		{
			cout << "Adding: " << number << endl;
			// The number isn't in the vector, adding it
			returnValue.push_back(number);
		}
		else
		{
			cout << number << " is already in the vector" << endl;
			// Number is already in the vector
			// Theoretically, the random could forever give us a number that is in the vector
			// Instead, we will either add or remove one number from
			int plusOrMinus = rand() % 2; // 0 substracts, 1 adds
			int changedNumber = number;
			if (plusOrMinus == 0)
			{
				cout << "minus" << endl;
				while (1)
				{
					changedNumber -= 1;
					cout << "Number: " << number << ", changedNumber: " << changedNumber << endl;
					if (changedNumber <= 0) // If we go too low, go to max
						changedNumber = 39;
					if (find(returnValue.begin(), returnValue.end(), changedNumber) == returnValue.end())
					{
						// The number isn't in the vector, adding it
						returnValue.push_back(changedNumber);
						cout << number << " -changed to " << changedNumber << endl;
						break;
					}
				}
			}
			else // Add
			{
				cout << "plus" << endl;
				while (1)
				{
					changedNumber += 1;
					cout << "Number: " << number << ", changedNumber: " << changedNumber << endl;
					if (changedNumber > 39) // If we go too high, go to min
						changedNumber = 1;
					if (find(returnValue.begin(), returnValue.end(), changedNumber) == returnValue.end())
					{
						// The number isn't in the vector, adding it
						returnValue.push_back(changedNumber);
						cout << number << " +changed to " << changedNumber << endl;
						break;
					}
				}
			}
		}
	}
	
	// Sorting the returnValue
	std::sort(returnValue.begin(), returnValue.end());
	
	return returnValue;
}

int main()
{
	vector<int> test = newLotteryNumbers();
	for (int i = 0; i < test.size(); ++i)
	{
		cout << test.at(i) << endl;
	}
}