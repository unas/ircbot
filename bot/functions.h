#ifndef FUNCTIONS_H
#define FUNCTIONS_H

enum datePart
{
	YEAR,
	MONTH,
	DAY,
	HOUR,
	MINUTE,
	SECOND
};

std::vector<std::string>str2vec(std::string, char);
std::string str2lower(std::string);
std::string trim(std::string);
std::string convertInt(int number);
std::vector<int>strVec2IntVec(std::vector<std::string> str);
void trim(std::string &str, char chr);
std::string cleanLine(std::string line);
std::string findWordsInLine(std::string line, std::vector<std::string>words);
bool charIsSpaceOrSpecial(char); // Used in findWordsInLine to determine if char is a space or other word separator char

// E.g. 2013-01-09 17:57:34
int getDatePart(datePart part, std::string date);
int getWeekDay(std::string date);
std::string getCurrentTime();

#endif