#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <time.h>
#include <cstdlib>
#include <fcntl.h>
#include <vector>
#include <sqlite3.h>
#include <stdio.h>
#include "botbase.h"

namespace IB
{
	// Presenting functions from the functions.cpp
	#include "functions.h"

	BotBase::BotBase(): dbfile_("botbase.db")
	{
		szErrMsg_ = 0;
		openDatabase();
	}

	BotBase::BotBase(std::string file): dbfile_(file)
	{
		szErrMsg_ = 0;
		openDatabase();
	}
	
	BotBase::~BotBase()
	{
		sqlite3_close(db_);
	}
	
	bool BotBase::openDatabase()
	{
		// open database
		int rc = sqlite3_open(dbfile_.c_str(), &db_);
		
		if(rc)
		{
			std::cout << "Can't open database\n";
			return false;
		} 
		else 
		{
			std::cout << "Open database successfully\n";
			return true;
		}
	}

	bool BotBase::addUserOldCount(std::string user)
	{
		// Checking if the user is new, creating if necessary
		if (!doesUserExist(user))
			createUser(user);
		
		
		std::string sql = "UPDATE user SET wanhaCount=wanhaCount + 1 WHERE username='" +
						user +
						"';";
		runSqlCommand(sql); // Running the sql command
	}
	
	void BotBase::createUser(std::string user)
	{
		std::string sql = "INSERT INTO user(ID, username, wanhaCount) VALUES(NULL, '" +
						user + "', 0);";
		
		runSqlCommand(sql);
	}
	/*
	void BotBase::createNewUserSaidWord(std::string user, std::string word)
	{
		int userID = getUserID(user);
		int wordID = getForbiddenWordID(word);
		std::string sql = "INSERT INTO spokenWord(forbiddenWordID, userID, strikes) " + 
						(std::string)"VALUES(" + convertInt(wordID) + ", " + 
						convertInt(userID) + (std::string)", 0);";
		runSqlCommand(sql);
	}
	*/
	bool BotBase::addNewLink(std::string user, std::string url)
	{
		// We must first look if the user has pasted a link from before
		// If not, create user. Otherwise link the url to the existing user
		std::cout << "Does user exist: " << user << std::endl;
		if (!doesUserExist(user))
		{
		    	std::cout << "User does not exist, creating...";
			createUser(user);
		}
		
		std::string sql = (std::string)"INSERT INTO link(ID, url, time, linked, userID) " +
						(std::string)"SELECT NULL, '" + url + (std::string)"', DATETIME('NOW', 'localtime'), 0, ID " +
						(std::string)"FROM user WHERE username='" + user + (std::string)"';";
		runSqlCommand(sql);
		
		std::cout << "SQLITE: Added url: \"" + url + "\"" << std::endl;
		std::cout << "SQLITE Output: " << std::endl;
		for (int i = 0; i < sqlResult_.size(); ++i)
		{
			for (int j = 0; j < sqlResult_.at(i).size(); ++j)
			{
				std::cout << sqlResult_.at(i).at(j) << " - ";
			}
			std::cout << std::endl << std::endl;
		}
	}
	/*
	void BotBase::removeUserStrikes(std::string user)
	{
		std::string sql = "UPDATE user SET strikes=0 WHERE username='" + user + (std::string)"';";
		runSqlCommand(sql);
		
		// DEBUG
		std::cout << "SQLITE: Removed strikes from user: \"" + user + "\"" << std::endl;
		std::cout << "SQLITE Output: " << std::endl;
		for (int i = 0; i < sqlResult_.size(); ++i)
		{
			for (int j = 0; j < sqlResult_.at(i).size(); ++j)
			{
				std::cout << sqlResult_.at(i).at(j) << " - ";
			}
			std::cout << std::endl << std::endl;
		}
	}
	*/
	
	bool BotBase::addNewLink(std::string user, std::string url, std::string datetime)
	{
		// We must first look if the user has pasted a link from before
		// If not, create user. Otherwise link the url to the existing user
		std::cout << "Does user exist: " << user << std::endl;
		if (!doesUserExist(user))
		{
		    	std::cout << "User does not exist, creating...";
			createUser(user);
		}
		
		std::string sql = (std::string)"INSERT INTO link(ID, url, time, linked, userID) " +
						(std::string)"SELECT NULL, '" + url + (std::string)"', '" + datetime + "', 0, ID " +
						(std::string)"FROM user WHERE username='" + user + (std::string)"';";
		runSqlCommand(sql);
		
		std::cout << "SQLITE: Added url: \"" + url + "\"" << std::endl;
		std::cout << "SQLITE Output: " << std::endl;
		for (int i = 0; i < sqlResult_.size(); ++i)
		{
			for (int j = 0; j < sqlResult_.at(i).size(); ++j)
			{
				std::cout << sqlResult_.at(i).at(j) << " - ";
			}
			std::cout << std::endl << std::endl;
		}
	}
	
	bool BotBase::doesUserExist(std::string user)
	{
		std::string sql = "SELECT * FROM user WHERE username='" + user + "';";
		runSqlCommand(sql);
		
		//std::cout << "KOKO SQL: " << sqlResult_.size() << std::endl;
		
		if (sqlResult_.size() == 0) // If nothing was returned, then the user doesn't exist
			return false;
		
		return true;
	}
	/*
	bool BotBase::hasUserSaidWordBefore(std::string user, std::string word)
	{
		int userID = getUserID(user);
		int wordID = getForbiddenWordID(word);
		std::string sql = "SELECT strikes FROM spokenWord WHERE forbiddenWordID = " + 
							convertInt(wordID) + " AND userID = " + convertInt(userID) + (std::string)";";
							
		if (sqlResult_.size() == 0) // If nothing was returned, then the user hasn't said this word before
			return false;
		
		return true;
	}
	*/
	bool BotBase::isLinkOld(std::string url)
	{
		std::string sql = "SELECT url FROM link WHERE url='" + url +"';";
		
		runSqlCommand(sql); // Stores the sql result to the sqlResult_ variable in to the class private
		
		if (sqlResult_.size() == 0)
			return false;
		
		return true;
	}
	void BotBase::runSqlCommand(std::string sqlCommand)
	{
	    std::cout << "Running sql command \"" << sqlCommand << "\""<< std::endl;
		sqlite3_prepare( db_, sqlCommand.c_str(), -1, &stmt_, NULL ); // Set the command
		sqlite3_step( stmt_ ); // Evaluates the command, I don't know, sqlite is fucking retarded
		
		// Making sure the vector is empty
		sqlResult_.clear();
		
		int count = 0; // So we know what line we are at with the sql text
		while( sqlite3_column_text( stmt_, 0 ) )
		{
			sqlResult_.push_back(std::vector<std::string>()); // Puts a new index to the vector
			for( int i = 0; i < sqlite3_column_count(stmt_); i++ )
			{
				sqlResult_.at(count).push_back((char *)sqlite3_column_text( stmt_, i ));
			}
			++count;
			sqlite3_step(stmt_);
		}
	}

	std::string BotBase::getOriginalUser(std::string url)
	{
		std::string sql = "SELECT username FROM user INNER JOIN link ON user.ID=link.userID WHERE link.url='" +
						url + "';";
		
		runSqlCommand(sql);
		
		return getSqlResultText();
	}
	
	std::string BotBase::getOriginalDate(std::string url)
	{
		std::string sql = "SELECT time FROM link INNER JOIN user ON link.userID=user.ID WHERE link.url='" + 
						url + "';";
		
		runSqlCommand(sql);
		
		return getSqlResultText();
	}
	
	std::string BotBase::getUserWanhaCount(std::string user)
	{
		std::string sql = "SELECT wanhaCount FROM user WHERE username='" + 
						user + "';";
		
		// Running the sql command
		runSqlCommand(sql);
		
		return getSqlResultText();
	}
	
	std::string BotBase::getUserKarmaCount(std::string user)
	{
		std::string sql = "SELECT karmaCount FROM user WHERE username='" +
						user + "';";
		
		// Running the sql command
		runSqlCommand(sql);
		
		return getSqlResultText();
	}
	/*
	void BotBase::addWordCountForUser(std::string user, std::string word)
	{
		int userID = getUserID(user);
		int wordID = getForbiddenWordID(word);
		std::string sql = "UPDATE spokenWord SET strikes = strikes + 1 WHERE userID = " + 
							convertInt(userID) + " AND forbiddenWordID = " +
							convertInt(wordID) + ";";
		runSqlCommand(sql);
	}
	*/
	/*
	void BotBase::addStrikesForUser(std::string user)
	{
		std::string sql = "UPDATE user SET strikes = strikes + 1 WHERE username = '" + user + "';";
		runSqlCommand(sql);
	}
	*/
	
	bool BotBase::addKickedUs(std::string user)
	{
		// Checking if the user exists
		if (!doesUserExist(user))
			createUser(user);
	
		std::string sql = "UPDATE user SET kickedUs = kickedUs + 1 WHERE username='" + 
						user + "'";
		
		runSqlCommand(sql);
		
		return true;
	}
	/*
	std::vector<std::string> BotBase::getForbiddenWords()
	{
		std::string sql = "SELECT word FROM forbiddenWord;";
		
		runSqlCommand(sql);
		
		// Getting the values from the database
		std::vector<std::string>returnValue;
		for(int i = 0; i < sqlResult_.size(); ++i)
		{
			for (int j = 0; j < sqlResult_.at(i).size(); ++j)
			{
				returnValue.push_back(trim(sqlResult_.at(i).at(j)));
			}
		}
		
		// returning a list of forbidden words
		return returnValue;
	}
	*/
	/*
	// If a user says a forbidden word
	int BotBase::addUserStrike(std::string user, std::string word)
	{
		// Checking if user account exists, creating if not
		if (doesUserExist(user))
			createUser(user);
		
		// Checking if the user has said the word before, creating if not
		if (hasUserSaidWordBefore(user, word))
			createNewUserSaidWord(user, word);
		
		// Adding for the user, that they said a forbidden word
		addWordCountForUser(user, word);
		
		// adding a strike for the username
		addStrikesForUser(user);
		
		// Checking how many strikes the user has and returning the value
		return getUserStrikeAmount(user);
	}
	*/
	
	
	std::vector<std::string> BotBase::hasSomeoneKickedUsInThePast()
	{
		std::string sql = "SELECT username FROM user WHERE kickedUs>0;";
		
		runSqlCommand(sql);
		
		std::vector<std::string>returnValue;
		for (int i = 0; i < sqlResult_.size(); ++i)
		{
			for (int j = 0; j < sqlResult_.at(i).size(); ++j)
			{
				returnValue.push_back(sqlResult_.at(i).at(j));
			}
		}
		
		return returnValue;
	}
	
	void BotBase::resetKickStatus(std::string user)
	{
		std::string sql = "UPDATE user SET kickedUs=0 WHERE username='" +
						user + "';";
		
		runSqlCommand(sql);
	}
	int BotBase::getUserKickAmount(std::string user)
	{
		std::string sql = "SELECT kickedUs FROM user WHERE username = '" + user + "';";
		
		runSqlCommand(sql);
		
		int returnValue = 0;
		if (sqlResult_.size() > 0)
			if (sqlResult_.at(0).size() > 0)
				returnValue = atoi(sqlResult_.at(0).at(0).c_str());
		
		return returnValue;
	}
	/*
	int BotBase::getUserStrikeAmount(std::string user)
	{
		std::string sql = "SELECT strikes FROM user WHERE username = '" + user + "';";
		runSqlCommand(sql);
		
		int returnValue = 0;
		if (sqlResult_.size() > 0)
			if (sqlResult_.at(0).size() > 0)
				returnValue = atoi(sqlResult_.at(0).at(0).c_str());
		
		return returnValue;
	}
	*/
	/*
	int BotBase::getForbiddenWordID(std::string word)
	{
		std::string sql = "SELECT ID FROM forbiddenWord WHERE word = '" + word + "';";
		
		runSqlCommand(sql);
		
		int returnValue = 0;
		if (sqlResult_.size() > 0)
			if (sqlResult_.at(0).size() > 0)
				returnValue = atoi(sqlResult_.at(0).at(0).c_str());
		
		return returnValue;
	}
	*/
	int BotBase::getUserID(std::string user)
	{
		std::string sql = "SELECT ID FROM user WHERE username = '" + user + "';";
		
		runSqlCommand(sql);
		
		int returnValue = 0;
		if (sqlResult_.size() > 0)
			if (sqlResult_.at(0).size() > 0)
				returnValue = atoi(sqlResult_.at(0).at(0).c_str());
		
		return returnValue;
	}
	
	std::string BotBase::getLottoUpdateTime()
	{
		std::string sql = "SELECT time FROM lotto WHERE ID=1;";
		runSqlCommand(sql);
		
		return getSqlResultText();
	}
	
	std::string BotBase::getSqlResultText()
	{
		if (sqlResult_.size() == 0 || sqlResult_.size() > 1)
		{
			std::cout << "ERROR1: THERE ARE MULTIPLE RESULTS (or none)!" << std::endl;
			std::cout << "CHECK DATABASE: " << sqlResult_.size() << std::endl;
			return "";
		}
		
		if (sqlResult_.at(0).size() == 0 || sqlResult_.at(0).size() > 1)
		{
			std::cout << "ERROR2: THERE ARE MULTIPLE RESULTS (or none)!" << std::endl;
			std::cout << "CHECK DATABASE: " << sqlResult_.at(0).size() << std::endl;
			return "";
		}
		
		return sqlResult_.at(0).at(0);
	}
	
	std::vector<int> BotBase::getLottoNumbers()
	{
		std::vector<int>returnValue;
		int tempNumber = 0;
		// Getting the lotterynumbers, one at a time
		for (int i = 1; i <= 7; ++i)
		{
			std::string sql = "SELECT n" + convertInt(i) + " FROM lotto WHERE ID=1;";
			runSqlCommand(sql);
			if (sqlResult_.size() > 0)
				if (sqlResult_.at(0).size() > 0)
					tempNumber = atoi(sqlResult_.at(0).at(0).c_str());
			returnValue.push_back(tempNumber);
		}
		
		return returnValue;
	}
	
	bool BotBase::setLottoNumbers(std::vector<int>numbers)
	{
		// Making sure there is a right amount of numbers
		if (numbers.size() != 7)
		{
			std::cout << "Can't create new lotto numbers, wrong amount: " << numbers.size() << std::endl;
			return false;
		}
		// Making sure the numbers are allowed (1 - 39)
		for (int i = 0; i < numbers.size(); ++i)
		{
			if (numbers.at(i) < 1 || numbers.at(i) > 39)
			{
				std::cout << "Can't create new lotto numbers, wrong number: " << (i + 1) << " - " << numbers.at(i) << std::endl;
				return false;
			}
		}
		// Adding numbers
		std::string sql = "UPDATE lotto SET ";
		for (int i = 0; i < numbers.size(); ++i)
		{
			sql += "n" + convertInt(i + 1) + "=" + convertInt(numbers.at(i)) + ", ";
		}
		sql += "time=datetime('now', 'localtime') WHERE ID=1;";
		runSqlCommand(sql);
		return true;
	}
	
	bool BotBase::addKarmaPoints(std::string user, bool addPoints)
	{
		// Checking if the user is new, creating if necessary
		if (!doesUserExist(user))
			createUser(user);
		std::string sql = "";
		if (addPoints)
		{
			sql = "UPDATE user SET karmaCount=karmaCount + 1 WHERE username='" +
							user + "';";
		}
		else
		{
			sql = "UPDATE user SET karmaCount=karmaCount - 1 WHERE username='" +
							user + "';";
		}
		
		runSqlCommand(sql);
	}
}
