#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <time.h>
#include <cstdlib>
#include <fcntl.h>
#include <vector>
#include <algorithm>
#include "ircbot.h"
#include "botbase.h"
//#include "functions.h"

namespace IB
{
	// Presenting functions from the functions.cpp
	#include "functions.h"

	IrcBot::IrcBot(): server_("irc.quakenet.org"), port_("6667"), nick_("unasBOT"), user_("unasBOT")
	{
		connected_ = false;
		std::cout << "Using default values..." << std::endl;
		operator_ = false;
		gameShoutTime = 0;
		
		// Setting Master values
		Master_ = "";
		MasterChangable_ = true;
		MasterIsLaw_ = true;
		MasterPassword_ = "yoda";
		userDisabledMaster_ = "";
	}
	
	IrcBot::IrcBot(std::string server, std::string port, std::string nick, std::string user) 
	: server_(server), port_(port), nick_(nick), user_(user)
	{
		connected_ = false;
		operator_ = false;
		gameShoutTime = 0;
		
		// Setting Master values
		Master_ = "";
		MasterChangable_ = true;
		MasterIsLaw_ = true;
		MasterPassword_ = "yoda";
		userDisabledMaster_ = "";
	}
	
	IrcBot::~IrcBot()
	{
		close(sockNum_);
	}
	
	void *get_in_addr(struct sockaddr *sa)
{
	if (sa->sa_family == AF_INET) {
		return &(((struct sockaddr_in*)sa)->sin_addr);
	}

	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}
	
	bool IrcBot::connectServer()
	{
		// Clearing servinfo
		memset(&hints_, 0, sizeof hints_);
		
		//setup hints
		hints_.ai_family = AF_INET;
		hints_.ai_socktype = SOCK_STREAM; // TCP stream sockets
		
		// Getting info of the server to the structs, if 0 then all okay
		if ( getaddrinfo(server_.c_str(), port_.c_str(), &hints_, &servinfo_) != 0 )
		{
			std::cout << "Could not connect to server, exiting..." << std::endl;
			return false;
		}
		
		// setting up the socket and connecting
		int connected;
		struct addrinfo *i; // Creating a copy of servinfo which we can manipulate
		char s[INET6_ADDRSTRLEN];
		for (i = servinfo_; i != NULL; i = i->ai_next)
		{
			sockNum_ = socket(i->ai_family, i->ai_socktype, i->ai_protocol);
			if(sockNum_ == -1) continue; // This socket was no good, trying the next one
			
			// trying to connect to the server (socket) we just chose
			connected = connect(sockNum_, i->ai_addr, i->ai_addrlen);
			if (connected == -1)
			{
				// no good, next socket
				close(sockNum_);
				continue;
			}
			break;
		}
		if (connected == -1)
		{
			std::cout << "Could not connect to server, exiting..." << std::endl;
			return false;
		}
		inet_ntop(i->ai_family, get_in_addr((struct sockaddr *)i->ai_addr), s, sizeof s);
		
		return true;
	}
	
	void IrcBot::registerUsername()
	{
		read();
		read();
		write("USER " + user_ + " d d " + user_);
		write("NICK " + user_);
	}
	
	void IrcBot::joinChannel(std::string channel, bool waitForConnect)
	{
		// Making sure the channel name is given
		if (channel.size() == 0)
		{
			std::cout << "Channel name can't be empty!" << std::endl;
			return;
		}
		
		if (waitForConnect)
		{
			while(true)
			{
				read();
				// Looking for the message that we are connected to the server
				if (ircText_.find("221 " + user_ + " +i") != -1)
					break;
				
				// Checking for ping
				checkForPing();
			}
		}
		
		// Adding the # infront of the channel name, if it is missing
		if (channel.at(0) != '#')
			channel = "#" + channel;
		
		write("JOIN " + channel);
		channel_ = channel;
	}
	
	std::string IrcBot::getUser(std::string line)
	{
		int userEndIndex = line.find('!');
		if (userEndIndex == -1) // No "!" found, well atleast there isn't a nick in this line
			return "";
		
		std::string user = line.substr(1, userEndIndex - 1);
		
		return user;
	}
	
	std::string IrcBot::getLineThatUserSaid(std::string line)
	{
		// The text that the user says comes after the second ":"
		// ":Mantsa!~mantsa@a91-152-165-213.elisa-laajakaista.fi PRIVMSG #08I220C :pelaan sit"
		int index = line.find(channel_ + " :");
		if (index != -1)
		{
			std::string returnValue = "";
			returnValue = line.substr(index + channel_.size() + 2);
			returnValue = returnValue.substr(0, returnValue.size() - 1); // Removing the newline from the end (\n)
			return returnValue;
		}
		
		return "";
	}
	
	std::string IrcBot::read()
	{
		const int bufSize = 5000;
		
		// Getting the text from the irc server
		char buffer [ bufSize ];
		std::string str = "";
		memset(buffer, 0, bufSize); // Putting a zero to every index
		// Getting the text from the socket (server), putting  it to the buffer array
		recv(sockNum_, buffer, bufSize - 1, 0); 
		str = buffer; // Putting the string to the string that will be returned
		
		ircText_ = str; // The private variable ircText will hold the current input value
		
		std::cout << str << std::endl;
		
		return str;
	}
	
	bool IrcBot::checkForPing()
	{
		int pingIndex = ircText_.find( "PING :", 0);
		if (pingIndex != -1) // If found. If not, then -1 is returned
		{
			std::string pong = "PONG :" + ircText_.substr(6,-1);
			write(pong);
			
			// Returning true, so now we know that there was a ping and it was answered to
			return true;
		}
		return false;
	}

	void IrcBot::write(std::string line)
	{
		bool doNotAddNewline = false;
		
		// If the user wants, we will add the newline to the end, if it is missing
		if (doNotAddNewline == false)
		{
			// To this line is added the missing newline characters.
			std::string addStr = "";
			
			// If \r is in the last on in the index before that
			if (line.at(line.size() - 1) != '\r' && line.at(line.size() - 2) != '\r' )
				addStr += "\r";
			
			if (line.at(line.size() - 1) != '\n' && line.at(line.size() - 2) != '\n' )
				addStr += "\n";
			
			line += addStr;
		}
	
		// Sending the text to the socket (server), the line is sent as a char array (c_str())
		send(sockNum_, line.c_str(), line.size(), 0);
		
		std::cout << "Command sent: " << line << std::endl;
	}
	
	void IrcBot::sayToChannel(std::string line)
	{
		write("PRIVMSG " + channel_ + " :" + line);
	}
	
	void IrcBot::sayToChannel(std::string line, std::string user)
	{
		write("PRIVMSG " + channel_ + " :" + user + ": " + line);
	}
	
	void IrcBot::sayToUser(std::string line, std::string user)
	{
		write("PRIVMSG " + user + " :" + line);
	}
	
	bool IrcBot::findUsersOnChannel()
	{
		write("NAMES " + channel_);
		
		// Updating the text from the server now that we asked for the users
		read();
		
		// Splitting the lines to a vector
		std::vector<std::string>userLines = str2vec(ircText_, '\n');
		
		int namesIndex = -1;
		for (int i = 0; i < userLines.size(); ++i)
		{
			if (userLines.at(i).find("353 " + user_ + " @ " + channel_) != -1
				|| userLines.at(i).find("353 " + user_ + " = " + channel_) != -1)
			{
				namesIndex = i;
				break;
			}
		}
		
		// We should only continue if the line with the usernames was found
		if (namesIndex >= 0)
		{
			// Looking for the index where our username is presented and after that the others
			int startIndex = userLines.at(namesIndex).find(":" + user_);
			// With the help of substr, we will cut out the beginning, and save only the user names to the string
			std::string users = userLines.at(namesIndex).substr(startIndex + 2 + user_.size());
			// Removing the last part from the usernames, the last char contains \n
			users = users.substr(0, users.size() - 1);
			
			// Splitting the usernames from the string directly to the vector
			names_ = str2vec(users, ' ');
			
			// Removing "@" and "+" signs from the names
			for (int i = 0; i < names_.size(); ++i)
			{
				// Making sure that we don't crash the program by poking indexes that don't exist
				if (names_.at(i).size() == 0)
					continue;
				
				if (names_.at(i).at(0) == '@' || names_.at(i).at(0) == '+')
					names_.at(i) = names_.at(i).substr(1);
			}
			
			if (names_.size() > 0)
				return true;
		}
		
		// Names weren't found
		return false;
	}
	
	bool IrcBot::removeUser(std::string user)
	{
		std::vector<std::string>::iterator it;
		
		// looking for the user that just left
		it = find(names_.begin(), names_.end(), user);

		// Only erase user if it was actually found from the vector
		if (it - names_.begin() != names_.size())
		{
			names_.erase(it);
			return true;
		}
		
		return false;
	}
	
	void IrcBot::addUser(std::string user)
	{
		names_.push_back(user);
	}
	
	bool IrcBot::changeNick(std::string oldNick, std::string newNick)
	{
		std::vector<std::string>::iterator it;
		
		// looking for the user that changed their nick
		it = find(names_.begin(), names_.end(), oldNick);

		// Only change the nick if it was found from the vector
		if (it - names_.begin() != names_.size())
		{
			*it = newNick;
			return true;
		}
		
		return false;
	}
	
	// Trying, don't use
	const IrcBot& IrcBot::operator >> (std::string& s) const
	{
		const int MAXRECV = 500;
	
		char buf [ MAXRECV + 1 ];
		s = "";
		memset ( buf, 0, MAXRECV + 1 ); // Zero to every index
		::recv ( sockNum_, buf, MAXRECV, 0 );
		s = buf;
		std::cout << ">> " + s << "\n";
		return *this;
	}

	const IrcBot& IrcBot::operator << (const std::string& s) const
	{
		::send ( sockNum_, s.c_str(), s.size(), 0 );
		std::cout << "<< " + s << "\n";
	}
	
	void IrcBot::printUsers()
	{
		std::cout << "USERS ON CHANNEL:" << std::endl;
		for (int i = 0; i < names_.size(); ++i)
		{
			std::cout << names_.at(i) << std::endl;
		}
		std::cout << "END" << std::endl;
	}

	bool IrcBot::kickUser(std::string user, std::string reason)
	{
		if (!isUserOnChannel(user)) // Can't kick if they aren't here
			return false;
		
		write("KICK " + channel_ + " " + user + " " + reason);
		
		// Remove user from list
		removeUser(user);
		
		return true;
	}
	
	bool IrcBot::isUserOnChannel(std::string user)
	{
		std::vector<std::string>::iterator it;
		it = find (names_.begin(), names_.end(), user);
		
		if (it == names_.end())
			return false;
		
		return true;
	}
}