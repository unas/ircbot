#include <iostream>
#include <cstdlib>
#include "ircbot.h"

using namespace std;


int main(int argc, char* argv[])
{
	// Creating the ircbot creature
	IB::IrcBot bot("irc.quakenet.org", "6667", "iana", "iana");
	
	// Connecting to the ircServer
	if (!bot.connectServer() )
		return 1;
	
	// Registering username on the server
	bot.registerUsername();
	// Join to channel, true -> waits for server to have fully connected
	bot.joinChannel("#08I220C", true);
	bot.read();
	
	// Finding all the users on the channel and saving their nicks
	bot.findUsersOnChannel();
	
	while(true)
	{
		bot.read();
		//checking for ping
		if (!bot.checkForPing())
		{
			// This line was not a ping, then we will analyze that line
			bot.parseLine();
		}
	}
}

