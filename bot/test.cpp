#include <algorithm>
#include <arpa/inet.h>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <errno.h>
#include <fcntl.h>
#include <iostream>
#include <netdb.h>
#include <netinet/in.h>
#include <signal.h>
#include <sstream>
#include <stdio.h>  
#include <stdlib.h>
#include <string>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>
#include <vector>

using namespace std;

enum datePart
{
	YEAR,
	MONTH,
	DAY,
	HOUR,
	MINUTE,
	SECOND
};

std::string convertInt(int number)
{
   stringstream ss;//create a stringstream
   ss << number;//add number to the stream
   return ss.str();//return a string with the contents of the stream
}

std::vector<std::string>str2vec(std::string str, char delimiter)
{
	// The vector where the lines will be stored and then returned
	std::vector<std::string>returnValue;
	
	while (true)
	{
		// Find the first index with a newline
		int newlineIndex = str.find(delimiter);
		if (newlineIndex == -1)
		{
			// This is the last line
			returnValue.push_back(str);
			break;
		}
		// Taking every char from the beginning, all the way to the newline
		returnValue.push_back(str.substr(0, newlineIndex));
		
		// Removing the first line, including the newline
		if (str.size() > newlineIndex + 1) // Making sure there is still something left after the newline
		{
			str = str.substr(newlineIndex + 1);
		}
		else
			break;
	}
	
	return returnValue;
}

std::vector<int>strVec2IntVec(std::vector<std::string> str)
{
	std::vector<int>returnValue;
	for (int i = 0; i < str.size(); ++i)
	{
		int temp = atoi(str.at(i).c_str());
		returnValue.push_back(temp);
	}
	return returnValue;
}

int getDatePart(datePart part, std::string date)
{
	// converting "2013-01-09 17:57:34" to "2013-01-09-17-57-34"
	std::replace(date.begin(), date.end(), ' ', '-');
	std::replace(date.begin(), date.end(), ':', '-');

	std::vector<std::string>parts = str2vec(date, '-');
	std::vector<int>intParts = strVec2IntVec(parts);
	
	if (intParts.size() != 6)
	{
		std::cout << "The splitting of the date failed: " << intParts.size() << " " << date << std::endl;
		return -1;
	}
	if (part == YEAR)
		return intParts.at(0);
	if (part == MONTH)
		return intParts.at(1);
	if (part == DAY)
		return intParts.at(2);
	if (part == HOUR)
		return intParts.at(3);
	if (part == MINUTE)
		return intParts.at(4);
	if (part == SECOND)
		return intParts.at(5);
}

int getWeekDay(std::string date)
{
	int year = getDatePart(YEAR, date);
	int month = getDatePart(MONTH, date);
	int day = getDatePart(DAY, date);
	int hour = getDatePart(HOUR, date);
	int minute = getDatePart(MINUTE, date);
	int second = getDatePart(SECOND, date);
	
	cout << "DAY: " << day << endl;
	
	// Getting the weekday when lotterynumber was created. Day is 1-based, month is 0 based. Years since 1900
	std::tm time = {second, minute, hour, day, (month - 1), (year - 1900) };
	std::time_t timeConv = std::mktime(& time);
	std::tm const *time_out = std::localtime(& timeConv);
	int weekday = time_out->tm_wday; // Sunday = 0, monday = 1 etc.
	
	return weekday;
}

bool hasLotteryPassed(std::string lottoTime)
{
	// When will the lottery be drawn?
		
	struct tm lTime;
	//struct tm cTime;
	struct tm *lotteryDate;
	struct tm lDateTime;
	// initialize declared structs
	memset(&lTime, 0, sizeof(struct tm));
	//memset(&cTime, 0, sizeof(struct tm));
	//memset(&lotteryDate, 0, sizeof(struct tm));
	//memset(&lDateTime, 0, sizeof(struct tm));
	
	//time_t lT;
	//time_t cT;
	
	strptime(lottoTime.c_str(), "%Y-%m-%d %H:%M:%S", &lTime); // Converting database lottotime string to tm
	//strptime(currentTime.c_str(), "%Y-%m-%d %H:%M:%S", &cTime);
	time_t lT = mktime(&lTime); //database lotto time tm to time_t
	//cT = mktime(&cTime);
	
	int lottoWeekDay = getWeekDay(lottoTime); // gets the weekday when the database numbers were done
	//int currentWeekDay = getWeekDay(currentTime);
	
	int daysLeftToLotto = 6 - lottoWeekDay;
	
	cout << daysLeftToLotto << " " << lottoWeekDay << endl;
	
	// How many seconds left to saturday (from midnight to midnight)
	time_t lottoDrawDate = lT + (time_t)daysLeftToLotto * 24 * 60 * 60; 
	
	// If the lottoTime is saturday, then we just need to check if it is after 20:45:00
	if (daysLeftToLotto == 0) // The lotterynumbers were done on a saturday
	{
		int lottoClockTime = 20 * 60 * 60 + 45 * 60; // 20:45 turned into seconds
		int lTimeClockTime = lTime.tm_hour * 60 * 60 + lTime.tm_min * 60 + lTime.tm_sec; // The time that the lotto numbers were drawn
		
		if (lTimeClockTime > lottoClockTime) // If the lotterynumbers in the database is after 20:45
		{
			// The lottoDrawDate has to be moved forward with one week (7 days)
			lottoDrawDate += 7 * 24 * 60 * 60;
		}
	}
	
	lotteryDate = gmtime(&lottoDrawDate);
	std::string lotteryDateTimeStr = convertInt(lotteryDate->tm_year + 1900)
									+ "-" + convertInt(lotteryDate->tm_mon + 1)
									+ "-" + convertInt(lotteryDate->tm_mday)
									+ " 20"
									+ ":45"
									+ ":00";
	strptime(lotteryDateTimeStr.c_str(), "%Y-%m-%d %H:%M:%S", &lDateTime);
	
	/*
	cout << (lDateTime->tm_year + 1900)
			<< "-" << lDateTime->tm_mon + 1
			<< "-" << lDateTime->tm_mday
			<< " " << lDateTime->tm_hour
			<< ":" << lDateTime->tm_min
			<< ":" << lDateTime->tm_sec << endl;
	*/
	cout << (lDateTime.tm_year + 1900)
			<< "-" << lDateTime.tm_mon + 1
			<< "-" << lDateTime.tm_mday
			<< " " << lDateTime.tm_hour
			<< ":" << lDateTime.tm_min
			<< ":" << lDateTime.tm_sec << endl;
	// Now we have in lDateTime (tm) the date and time when the lottery numbers will be drawn for the
	// numbers that are right now in the database.
	// Now we check if the lottery drawing day has passed. If it has passed, we will return true.
	
	// Converting lDateTime to time_t
	time_t lDateTimeInSeconds = mktime(&lDateTime);
	// Time right now
	time_t currentTimeInSeconds = time(NULL);
	
	if (lDateTimeInSeconds < currentTimeInSeconds) // If the lottery drawing date has passed.
		return true;
	
	return false;
}

int main()
{
	string lottoDate = "2013-01-05 20:45:01";
	if (hasLotteryPassed(lottoDate))
		cout << "Yes" << endl;
	else
		cout << "No" << endl;
		
	return 0;
}



















