// To build this project
// g++ -l sqlite3 -o parse parseLinks.cpp botbase.cpp

#include <iostream>
#include <cstdlib>
#include <vector>
#include <string>
#include <bitset>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include "botbase.h"

using namespace std;

struct Link
{
	string url; // e.g. http://www.google.fi
	string user; // e.g. unas
	string datetime; // e.g. 2012-09-17 01:11:29
};

bool isLink(string line);
string getUser(string line);
string getUrl(string line);
string getTime(string line);
bool linkIsOld(string line, vector<Link>links);
bool dayChanged(string line);
vector<string>str2vec(string str, char delimiter);
string getCurrentDate(string line);
bool isLinkOld(string url, vector<Link>links);
string trim(string str);

int main()
{
	ifstream infile("08i220c_foranamo_20121018.log");
	string line = "";
	IB::BotBase DB("botbase2.db");
	
	// Where we store all the links
	vector<Link>links;
	
	int oldLinks = 0;
	
	string currentDate = "2009-11-23";
	
	while (getline(infile, line))
	{
		// Checking if this line changes the day
		if (dayChanged(line))
		{
			cout << "Day changed to: " << currentDate << endl;
			currentDate = getCurrentDate(line);
			continue;
		}
		
		// Looking for links from every line
		else if (isLink(line))
		{
			Link temp;
			temp.url = "";
			temp.user = "";
			temp. datetime = "";
			
			temp.url = getUrl(line);
			// Making sure that the link hasn't been added from before
			if (isLinkOld(temp.url, links))
			{
				cout << "Link is old: " << line << endl;
				continue;
			}
			
			temp.user = getUser(line);
			// Checking if the user has spaces, then we screwep up. The user
			// quit or joined and there was a link in the text.
			// Skipping this line
			int test = temp.user.find(" ");
			if (test != -1)
			{
				cout << "Skipping line: " << line << endl;
				continue;
			}
			
			string time = getTime(line);
			temp.datetime = currentDate + " " + time;
			
			// Trimming the info
			temp.user = trim(temp.user);
			temp.url = trim(temp.url);
			temp.datetime = trim(temp.datetime);
			
			
			if (currentDate.size() != 10)
			{
				cout << "CurrentDate is of wrong format: " << currentDate << endl;
				return -1;
			}
			
			// If any of the values are empty, then moving on
			if (temp.user == "" || temp.url == "" || temp.datetime == "")
				continue;
			
			// Changing some of the nicks to the correct ones
			if (temp.user == "Kristuz_")
				temp.user = "Kristuz";
			if (temp.user == "Ikkin_")
				temp.user = "Ikkin";
			if (temp.user == "mantsa")
				temp.user = "Mantsa";
			if (temp.user == "Ikkinn")
				temp.user = "Ikkin";
			if (temp.user == "ikki")
				temp.user = "Ikkin";
			if (temp.user == "dylle107")
				temp.user = "dylle";
			if (temp.user == "wzardd")
				temp.user = "samiy";
			if (temp.user == "Filter")
				temp.user = "filsuu";
			if (temp.user == "mantsa_")
				temp.user = "Mantsa";
			if (temp.user == "risto_")
				temp.user = "risto";
			if (temp.user == "Niki")
				temp.user = "Ikkin";
			if (temp.user == "ratamoh")
				temp.user = "ratamo";
			if (temp.user == "d-y-l-l-e")
				temp.user = "dylle";
			if (temp.user == "ratamo-risto-ni")
				temp.user = "ratamo";
			if (temp.user == "risto__")
				temp.user = "risto";
			if (temp.user == "lepy")
				temp.user = "lepya";
			if (temp.user == "Dylle108")
				temp.user = "dylle";
			if (temp.user == "iikki")
				temp.user = "Ikkin";
			if (temp.user == "unas900")
				temp.user = "unas";
			if (temp.user == "z-kk-")
				temp.user = "Ikkin";
			if (temp.user == "mantsa-work")
				temp.user = "Mantsa";
			if (temp.user == "Mantsa2")
				temp.user = "Mantsa";
			if (temp.user == "ikkin_")
				temp.user = "Ikkin";
			if (temp.user == "dylle_")
				temp.user = "dylle";
			if (temp.user == "dQ_")
				temp.user = "dQ";
			if (temp.user == "filsuu_")
				temp.user = "filsuu";
			if (temp.user == "xkj_")
				temp.user = "xkj";
			if (temp.user == "aratamo")
				temp.user = "ratamo";
			if (temp.user == "unas_")
				temp.user = "unas";
			if (temp.user == "dQ-")
				temp.user = "dQ";
			if (temp.user == "ikkin")
				temp.user = "Ikkin";
			if (temp.user == "risto-")
				temp.user = "risto";
			
			// Skipping the bots
			if (temp.user == "TiTeLAN") continue;
			if (temp.user == "TiTeLAN`") continue;
			if (temp.user == "iana") continue;
			if (temp.user == "unas_bot") continue;
			if (temp.user == "unasBOT") continue;
			if (temp.user == "vassykka") continue;
			
			/*
			int test1 = line.find("http://www.apple.com");
			if (test1 != -1 || temp.url == "http://www.apple.com")
			{
				string asd = "";
				cout << line << " - " << currentDate << " - " << temp.user << " - " << temp.url << " - " << temp.datetime << endl;
				cin >> asd;
			}
			*/
			
			// Adding the link to the vector
			links.push_back(temp);
		}
	}
	
	// Putting the links to the database
	for (int i = 0; i < links.size(); ++i)
	{
		DB.addNewLink(links.at(i).user, links.at(i).url, links.at(i).datetime);
	}
}

string trim(string str)
{
	// Removing space from beginning of string
	if (str.size() > 0)
	{
		while (true)
		{
			if(str.at(0) == ' ')
				str = str.substr(1);
			else
				break;
		}
	}
	// Removing the space from the end
	if (str.size() > 0)
	{
		while (true)
		{
			if(str.at(str.size() - 1) == ' ')
				str = str.substr(0, str.size() - 2);
			else
				break;
		}
	}
	
	return str;
}

bool isLinkOld(string url, vector<Link>links)
{
	for (int i = 0; i < links.size(); ++i)
	{
		if (links.at(i).url == url)
			return true;
	}
	return false;
}

string getCurrentDate(string line)
{
	string months[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
	string monthsNumber[] = { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" };

	// Okay, the day changed, now let's find the day, month and year
	vector<string>temp = str2vec(line, ' ');
	
	// Getting the day number
	string day = temp.at(5);
	
	string month = temp.at(4);
	string monthNumber = "";
	
	// Getting the string number value of the month
	for (int i = 0; i < 12; ++i)
	{
		if (months[i] == month)
		{
			monthNumber = monthsNumber[i];
		}
	}
	if (monthNumber == "")
		return "";
	
	// The year is in the last index. This is a must, depends on if the day was changed, or if the log was opened.
	string year = temp.at(temp.size() - 1);
	
	string returnValue = year + "-" + monthNumber + "-" + day;
	
	return returnValue;
}

bool dayChanged(string line)
{
	// "--- Log opened" or "--- Day changed" tells us that the day changed
	int index1 = line.find("--- Log opened");
	int index2 = line.find("--- Day changed");
	
	if (index1 == 0 || index2 == 0)
	{
		return true;
	}

	return false;
}

vector<string>str2vec(string str, char delimiter)
{
	// The vector where the lines will be stored and then returned
	vector<string>returnValue;
	
	while (true)
	{
		// Find the first index with a newline
		int newlineIndex = str.find(delimiter);
		if (newlineIndex == -1)
		{
			// This is the last line
			returnValue.push_back(str);
			break;
		}
		// Taking every char from the beginning, all the way to the newline
		returnValue.push_back(str.substr(0, newlineIndex));
		
		// Removing the first line, including the newline
		if (str.size() > newlineIndex + 1) // Making sure there is still something left after the newline
		{
			str = str.substr(newlineIndex + 1);
	}
		else
			break;
	}
	
	return returnValue;
}

string getTime(string line)
{
	// The time is from the beginning 5 chars
	return line.substr(0,5);
}

// Has the same link been pasted before
bool linkIsOld(string line, vector<Link>links)
{
	return true;
}

string getUser(string line)
{
	int start = line.find("<");
	// +2 because "<@unas" so the nick starts 2 indexes after the "<"
	start += 2;
	int end = line.find(">");
	end -= 1; // We don't want the ">" to the nick
	
	int nickLength = end - start + 1;
	
	string user = line.substr(start, nickLength);
	
	return user;
}

bool isLink(string line)
{		
	int urlStartIndex = line.find("http://");
	if (urlStartIndex == -1)
	{
		urlStartIndex = line.find("www.");
		if (urlStartIndex == -1)
			return false;
	}
	return true;
}

string getUrl(string line)
{ 
	int urlStartIndex = line.find("http://");
	if (urlStartIndex == -1)
	{
		urlStartIndex = line.find("www.");
	}
		
	// Find the first space after the link, if there is none, then there is nothing after the link
	int urlEndIndex = line.find(' ', urlStartIndex);
	
	std::string url = "";
	// If the space was found, then we know that the link ends there
	if (urlEndIndex != -1)
		url = line.substr(urlStartIndex, (urlEndIndex - urlStartIndex) + 1);
	else // There is no space after the link, so the link is the last thing on the line
		url = line.substr(urlStartIndex);
	
	// Someone might have put something after the link that is not allowed, like , or .
	// We will remove those
	for (int i = url.size() - 1; i > 0; --i)
	{
		if (url.at(i) == ',' || url.at(i) == '.' || url.at(i) == ':' || url.at(i) == ';' || 
			url.at(i) == '"' || url.at(i) == '(' || url.at(i) == ')' || url.at(i) == '\\' ||
			url.at(i) == '>' || url.at(i) == '<' || url.at(i) == ' ' || url.at(i) == '\n' ||
			url.at(i) == '\r' || url.at(i) == '/')
		{
			url.erase(i, 1); // Remove the last char
		}
		else
		{
			break;
		}
	}
	
	// Now, we have to add the http:// to the beginning (if needed)
	if (url.find("http://") != 0)
		url = "http://" + url;
	
	return url;
}
	
	/*
	
	// Okay, lets check if the link is old and stuff
	if (!botDB_.isLinkOld(url))
	{
		std::cout << "LINK IS NEW" << std::endl;
		botDB_.addNewLink(user, url);
	}
	else // Link is old
	{
		botDB_.addUserOldCount(user);
		std::string originalUser = botDB_.getOriginalUser(url);
		std::string originalDate = botDB_.getOriginalDate(url);
		std::string userWanhaCount = botDB_.getUserWanhaCount(user);
		
		std::string say = "WANHA! ";
		//sayToChannel("WANHA!", user);
		// Checking the user was the same one that originally pasted the link
		if (user == originalUser)
		{
			say += "You linked it yourself ";
		}
		else
		{
			say += "Originally linked by: " + originalUser + " ";
		}
		say += "on " + originalDate + ". ";
		
		say += "You have now linked " + userWanhaCount + " old link(s).";
		sayToChannel(say, user);
	}
	
	return false;
}
*/