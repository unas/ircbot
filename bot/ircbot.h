#ifndef IRCBOT_H
#define IRCBOT_H

#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <time.h>
#include <cstdlib>
#include <vector>
#include <ctime>
#include "botbase.h"

namespace IB
{
	/*
	struct words
	{
		std::string word;
		int warnings;
	};

	struct userWarnings
	{
		std::string user;
		int warnings;
		std::vector<words>saidWords;
	};
	*/

	class IrcBot
	{
		public:
			IrcBot();
			IrcBot(std::string server, std::string port, std::string nick, std::string user);
			~IrcBot();
			
			// Methods for connecting the bot to the server
			bool connectServer();
			void registerUsername();
			void joinChannel(std::string, bool);
			bool checkForPing();
			
			// Users
			bool findUsersOnChannel();
			bool removeUser(std::string);
			void addUser(std::string);
			bool changeNick(std::string, std::string);
			bool kickUser(std::string user, std::string reason);
			bool isUserOnChannel(std::string user);
			
			// Input/Output
			std::string read();
			void write(std::string);
			void sayToChannel(std::string line);
			void sayToChannel(std::string line, std::string user);
			void sayToUser(std::string line, std::string user);
			std::string getLineThatUserSaid(std::string line); // Returns "" if it wasn't a user that said something
			std::string getUser(std::string line); // Returns the username that said something
			
			void parseLine();
			
			// DEBUG
			void printUsers();
			
			// trying, don't use
			const IrcBot& operator >> (std::string&) const;
			const IrcBot& operator << (const std::string&) const;
		
		private:
			std::string server_;
			std::string port_;
			std::string nick_;
			std::string user_;
			std::string channel_;
			std::vector<std::string>names_; // The users on the channel
			
			// Youtube
			bool isYoutubeLink(std::string link);
			std::string convertYoutubeLink(std::string link);
			
			// Master
			std::string Master_; // The full name and address of our master
			bool MasterChangable_; // Can master be changed
			bool MasterIsLaw_; // Is master allowed to order us?
			std::string userDisabledMaster_; // The user who said that master can't be trusted
			std::string MasterPassword_;
			// Methods
			bool MasterCalling(std::string line, std::string MasterName);
			
			// Our database
			BotBase botDB_;
			
			// are we operator
			bool operator_;
			
			// Parsers (ircbot_parsers.cpp)
			bool hello();
			bool userLeft(std::string);
			bool userJoined(std::string);
			bool userChangedNick(std::string);
			bool linkPasted(std::string);
			bool otherBotTalking(std::string);
			bool wasIKicked(std::string);
			bool weGotOperatorStatus(std::string);
			bool weLostOperatorStatus(std::string);
			bool l4d(std::string);
			bool cs(std::string);
			bool karma(std::string); // Returns true if the user asked for their karmapoints
			bool wanha(std::string); // Returns true if the user asked for their wanhapoints
			bool askingForGame(std::string, std::string, std::vector<std::string>);
			bool userTalkingToBot(std::string);
			bool lotteryNumbers(std::string);
			std::string getPageTitle(std::string);
			std::string getWhatUserSaidToChannel(std::string); // Returns the text that a user said. Empty if not user
			std::string getUserNameFromLine(std::string); //Return the name of the user that is in this line.
			//bool userSaidApple(std::string);
			//void setWarningForUser(std::string user, std::string word);
			//bool tellUserAboutWarnings(std::string user, std::string word, int strikes); // true if kicked
			
			// Apple warnings
			//std::vector<userWarnings>userWarningList_;
			
			// Lotto
			bool hasLotteryPassed(std::string lottoTime);
			std::vector<int> newLotteryNumbers();
			
			// Extra
			void shouldWeKickSomeone();
			
			// Are we connected or not
			bool connected_;
			
			// The current line that was received from the server
			std::string ircText_;
			
			// tells us to which socket we are connected
			int sockNum_;
			
			// creating two structs, hints and servinfo
			struct addrinfo hints_, *servinfo_;
			
			// When was the last time we shouted for a game
			time_t gameShoutTime;
	};
}

#endif